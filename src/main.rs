use directories::ProjectDirs;
use rss::Channel;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs::{create_dir_all,File};
use std::error::Error;
use std::io;
use std::path::Path;
use structopt::StructOpt;
use simple_error::bail;

const APP_NAME: &str = "slowfeeder";
const CACHE_FNAME: &str = "cache.json";

#[derive(Deserialize, Serialize, Debug)]
enum RssItemStatus {
    Seen,
    Added,
}

#[derive(Deserialize, Serialize, Debug)]
struct RssItem {
    guid: String,
    link: String,
    status: RssItemStatus,
}

fn read_cache(path: &Path) -> Result<HashMap<String, RssItem>, io::Error> {
    let file = File::open(path)?;
    let reader = io::BufReader::new(file);
    let json = serde_json::from_reader(reader)?;
    Ok(json)
}

fn make_rss_item(item: &rss::Item) -> Option<RssItem> {
    Some(RssItem {
        guid: String::from(item.guid()?.value()),
        link: String::from(item.link()?),
        status: RssItemStatus::Seen,
    })
}

fn write_cache(cache: HashMap<String, RssItem>, path: &Path) -> Result<(), io::Error> {
    let file = File::create(path)?;
    serde_json::to_writer_pretty(io::BufWriter::new(file), &cache)?;
    Ok(())
}

#[derive(Deserialize,Debug)]
struct PocketAddResponse {
    access_token: String,
    username: String
}

fn add_to_pocket(url: &str, consumer_key: &str, access_token: &str)
                 -> Result<(), reqwest::Error>
{
    let pocket_add_url = "https://getpocket.com/v3/add";
    let client = reqwest::blocking::Client::new();
    
    let mut json = HashMap::new();
    json.insert("url", url);
    json.insert("consumer_key", consumer_key);
    json.insert("access_token", access_token);
    
    client.post(pocket_add_url)
        .header("X-Accept", "application/json")
        .json(&json)
        .send()?
        .error_for_status()?;
    Ok(())
}

fn add_item(link: &str, cfg: &AppConfig, dry_run: bool)
            -> Result<(), Box<dyn Error>>
{
    if dry_run {
        println!("ADDING (dry run) {}", link);
    } else {
        println!("ADDING {}", link);
        add_to_pocket(link, &cfg.consumer_key, &cfg.access_token)?;
    }
    Ok(())
}

fn do_add(mut cfg: AppConfig, cache: &mut HashMap<String, RssItem>, url: String,
          mark_added: bool) -> Result<(), Box<dyn Error>> {
    if cfg.feed_urls.contains(&url) {
        bail!("URL {} has already been added!", url);
    }

    println!("Loading RSS feed ...");
    let channel = Channel::from_url(&url)?;
        
    for item in channel.items() {
        match make_rss_item(item) {
            Some(mut rss_item) => if !cache.contains_key(&rss_item.guid) {
                println!("- Found item: {}", rss_item.link);
                if mark_added {
                    rss_item.status = RssItemStatus::Added;
                }
                cache.insert(rss_item.guid.clone(), rss_item);
            },
            None => println!("WARNING: unable to parse rss item for {}",
                             item.link().unwrap()),
        }
    }
    
    cfg.feed_urls.push(url);
    
    confy::store(APP_NAME, cfg)?;
    Ok(())
}

fn do_update(cfg: &AppConfig, cache: &mut HashMap<String, RssItem>, dry_run: bool)
             -> Result<(), Box<dyn Error>>
{
    let mut has_added = false;

    if cfg.feed_urls.len() == 0 {
        println!("No feeds have been configured, nothing to do...");
        return Ok(());
    }
    
    for feed_url in &cfg.feed_urls {
        let channel = Channel::from_url(&feed_url).unwrap();
        
        for item in channel.items() {
            match make_rss_item(item) {
                Some(mut rss_item) => if !cache.contains_key(&rss_item.guid) {
                    if !has_added {
                        add_item(&rss_item.link, &cfg, dry_run)?;
                        has_added = true;
                        if !dry_run {
                            rss_item.status = RssItemStatus::Added;
                        }
                    }
                    cache.insert(rss_item.guid.clone(), rss_item);
                },
                None => println!("WARNING: unable to parse rss item for {}",
                                 item.link().unwrap()),
            }
        }
    }
    
    // If nothing has been added, let's add a random old unadded one from
    // the old items
    if !has_added {
        for (_, item) in cache.iter_mut() {
            if let RssItemStatus::Seen = item.status {
                println!("No new items found, adding one from history...");
                add_item(&item.link, &cfg, dry_run)?;
                if !dry_run {
                    item.status = RssItemStatus::Added;
                }
                break;
            }
        }
    }
    
    Ok(())
}

#[derive(Default, Debug, Serialize, Deserialize)]
struct AppConfig {
    consumer_key: String,
    access_token: String,
    feed_urls: Vec<String>,
}

#[derive(StructOpt, Debug)]
#[structopt(name = APP_NAME)]
enum Opt {
    /// Add new feed
    Add {
        url: String,
        /// Mark all as already added
        #[structopt(short, long)]
        mark_added: bool,
    },
    /// Update feeds and add new or old items
    Update {
        /// Don't actually add to Pocket
        #[structopt(short, long)]
        dry_run: bool,
    },
}

fn main() {
    let cfg: AppConfig = match confy::load(APP_NAME) {
        Ok(cfg) => cfg,
        Err(e) => panic!("Error reading config: {}", e),
    };

    let proj_dirs = ProjectDirs::from("fi", "sjoberg", APP_NAME)
        .expect("Unable to find data directory!");
    
    let cache_dir = proj_dirs.data_dir();
    create_dir_all(cache_dir).expect("Unable to create data directory!");
    let cache_fname = cache_dir.join(CACHE_FNAME);

    let mut cache: HashMap<String, RssItem> = match read_cache(&cache_fname) {
        Ok(cache) => cache,
        Err(_) => HashMap::new(),
    };

    let result = match Opt::from_args() {
        Opt::Add{url, mark_added} => {
            do_add(cfg, &mut cache, url, mark_added)
        }
        Opt::Update{dry_run} => {
            do_update(&cfg, &mut cache, dry_run)
        }
    };

    match result {
        Ok(_) => (),
        Err(e) => println!("Error: {}", e.to_string()),
    };        

    if write_cache(cache, &cache_fname).is_err() {
        panic!("Unable to write cache to {:?}", &cache_fname);
    }
}
