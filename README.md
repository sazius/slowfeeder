# SlowFeeder

Simple command line application that fetches RSS feeds and adds new posts to a [Pocket](https://getpocket.com/) account. It has been designed to be launched daily as a cronjob. If no new posts are found, it will pick a random old post which hasn't been added yet to Pocket.

The problem that this mechanism solves is when you find a new blog with a huge backlog of old interesting posts, and you get overwhelmed with posts that you "should read". Just add the RSS feed to SlowFeeder, and it will feed old posts to you at a natural rate.

## Usage

First, you need to get a `consumer_key` and `access_token` for the Pocket API. Currently, this has to be done manually.

Then you can add new feeds like this:

    slowfeeder add https://www.theguardian.com/profile/evgeny-morozov/rss

Alternatively, if you wish to mark all the current posts as "added", i.e., they will not be among the "backlog" pool of posts that will added if no new posts are found, you can run like this

    slowfeeder add --mark-added http://feeds.feedburner.com/StudyHacks
    
To update all the feeds and add new posts, or pick one from the backlog, just run:

    slowfeeder update
    
You can run the above command as a cronjob, for example once per day.

## TODO

This program was written as a way for me to learn Rust. There is is still a lot of work to be done:

- [ ] add "auth" subcommand to do OAuth dance to get access_token (and
      fill to config file)
- [X] less cluttered error handling
- [ ] cache should probably be its own class or module in a separate
      file, to make code cleaner
